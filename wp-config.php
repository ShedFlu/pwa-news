<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_pwa_db' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 't65&!6gmvgtpew2&cgyp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aZ[=Z,^xX*Cm%s)8UnxE~#%>XF#/i=^ogJpMFw(bL_&gd#|#lmDC3:Z[Lq9;,Hr(' );
define( 'SECURE_AUTH_KEY',  'z|Um8AMLI(<I/+QZfGD(nxoSg6PAt`-<-ru)yWLBS=/aOglU+NXHS-)f&q cx:jH' );
define( 'LOGGED_IN_KEY',    '/k8r3_U!PDt4oHH@S}#{S`(Nq,to$S}StdFgCv8YW;Xol~yv3ReYr3LOpSn2OV(y' );
define( 'NONCE_KEY',        'FuXW0g[GXi_oxwI6()fn/hn%{q6?.G6v5|FbdR`>p1e*oXQJKCQI}h&&xghvPO2`' );
define( 'AUTH_SALT',        'TtI0G:6$M)Bx~|$Y2hSu~TrLN?s0B#}#H`c;5sq)Mme?XSyM=Ogh)iK38?oL()z3' );
define( 'SECURE_AUTH_SALT', ')gMWYoE4AhQY;iPo+pzlZ{+CPpw}UZjtLNudJ%q^Agi[ZE!7can^iP}a`}i~-l?R' );
define( 'LOGGED_IN_SALT',   'C6N:MQ&^wr->zJH{R.<zHyHDd2ja8kvequp6A $K#r:AChl)gS`O-0>wK2:tb_-A' );
define( 'NONCE_SALT',       'EP%Qa<E$Q4e1 hUMdP{ss]Yc5{QOFD@ffJ%yZ!K)Ud2D`jAB.x4Wb/XTXe/oOw#>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
